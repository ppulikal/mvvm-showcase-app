![Android CI](https://img.shields.io/bitbucket/pipelines/ppulikal/mvvm-showcase-app/master?style=flat-square)
![Pull Requests](https://img.shields.io/bitbucket/pr-raw/ppulikal/mvvm-showcase-app?style=flat-square)

![Language]()
![License]()


Brewery-MVVM-Showcase-app
=================
A showcase app illustrating Android development best practices with Android Jetpack, MVVM and Architecture Components


Getting Started
---------------
Use the `gradlew build` command or use "Import Project" in Android Studio.<br>

Features
--------------

  * [Master/Detail Flow][6]
  * [Navigation component ][0]
  * [Kotlin Coroutines][2]
  * [ViewModel][1]
  * [LiveData][3]
  * [Retrofit][4]
  * [Koin - Dependency Injection][7]
  * [Tests - Unit Tests & Espresso UI Tests][5]

[0]: https://developer.android.com/topic/libraries/architecture/navigation/
[1]: https://developer.android.com/topic/libraries/architecture/viewmodel
[2]: https://kotlinlang.org/docs/reference/coroutines-overview.html
[3]: https://developer.android.com/topic/libraries/architecture/livedata
[4]: https://github.com/square/retrofit
[5]: https://developer.android.com/training/testing/ui-testing/espresso-testing

[6]: https://developer.android.com/studio/projects/templates#MasterDetailFlow
[7]: https://insert-koin.io

Screenshots
-----------

![HomeScreen - Phone](screenshots/phone1.png "List Screen - Phone")
![DetailScreen - Phone](screenshots/phone2.png "Detail Screen - Phone")
<br>
<br>
![HomeScreen - Tablet](screenshots/tablet1.png "Home Screen - Tablet")

License
-------
MIT License

Copyright (c) 2020 Prasad Pulikal

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
