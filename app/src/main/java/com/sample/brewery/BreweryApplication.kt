package com.sample.brewery

import android.app.Application
import com.sample.brewery.di.appModules
import com.sample.brewery.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BreweryApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BreweryApplication)
            modules(listOf(appModules, networkModule))
        }
    }
}
