package com.sample.brewery.data

import com.sample.brewery.base.Response

class BeerItemsRepository(
    private val service: BeerItemsService
) {
    suspend fun getBeerItems(): Response<List<Beer>> {
        return try {
            return Response.success(service.getBeerItems())
        } catch (e: Exception) {
            handleError(e)
        }
    }

    private fun <R : Any> handleError(e: Exception): Response<R> {
        return Response.error(e.message ?: "Unknown Exception", null)
    }

}
