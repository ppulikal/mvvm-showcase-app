package com.sample.brewery.data

import retrofit2.Retrofit

class BeerItemsService(private val retrofit: Retrofit) : BeerItemsApi {
    private val beerItemsApi by lazy { retrofit.create(BeerItemsApi::class.java) }
    override suspend fun getBeerItems() = beerItemsApi.getBeerItems()
}

