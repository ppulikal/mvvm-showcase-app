package com.sample.brewery.data

import retrofit2.http.GET

interface BeerItemsApi {

    @GET("beers")
    suspend fun getBeerItems(): List<Beer>
}

