package com.sample.brewery.data

data class Beer(
    val abv: Double = 0.0,
    val attenuation_level: Double = 0.0,
    val boil_volume: BoilVolume = BoilVolume(),
    val brewers_tips: String = "",
    val contributed_by: String = "",
    val description: String = "",
    val ebc: Int = 0,
    val first_brewed: String = "",
    val food_pairing: List<String> = listOf(),
    val ibu: Double = 0.0,
    val id: Int = 0,
    val image_url: String = "",
    val ingredients: Ingredients = Ingredients(),
    val method: Method = Method(),
    val name: String = "",
    val ph: Double = 0.0,
    val srm: Double = 0.0,
    val tagline: String = "",
    val target_fg: Double = 0.0,
    val target_og: Double = 0.0,
    val volume: Volume = Volume()
)

data class BoilVolume(
    val unit: String = "",
    val value: Int = 0
)

data class Ingredients(
    val hops: List<Hop> = listOf(),
    val malt: List<Malt> = listOf(),
    val yeast: String = ""
)

data class Method(
    val fermentation: Fermentation = Fermentation(),
    val mash_temp: List<MashTemp> = listOf(),
    val twist: String = ""
)

data class Volume(
    val unit: String = "",
    val value: Int = 0
)

data class Hop(
    val add: String = "",
    val amount: Amount = Amount(),
    val attribute: String = "",
    val name: String = ""
)

data class Malt(
    val amount: Amount = Amount(),
    val name: String = ""
)

data class Amount(
    val unit: String = "",
    val value: Double = 0.0
)

data class Fermentation(
    val temp: Temp = Temp()
)

data class MashTemp(
    val duration: Int = 0,
    val temp: Temp = Temp()
)

data class Temp(
    val unit: String = "",
    val value: Int = 0
)
