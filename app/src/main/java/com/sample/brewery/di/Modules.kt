package com.sample.brewery.di

import com.sample.brewery.BuildConfig
import com.sample.brewery.data.BeerItemsApi
import com.sample.brewery.data.BeerItemsRepository
import com.sample.brewery.data.BeerItemsService
import com.sample.brewery.features.home.list.BeerListViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModules = module {
    single {
        BeerItemsRepository(get())
    }
    single {
        BeerItemsService(get())
    }
    viewModel {
        BeerListViewModel(get())
    }

}


val networkModule = module {
    factory { provideOkHttpClient(get()) }
    factory { provideLoggingInterceptor() }
    factory { provideBeerItemsApi(get()) }
    single { provideRetrofit(get()) }
}


fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.SERVER_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = HttpLoggingInterceptor.Level.BODY
    return logger
}

internal fun provideBeerItemsApi(retrofit: Retrofit): BeerItemsApi =
    retrofit.create(BeerItemsApi::class.java)

