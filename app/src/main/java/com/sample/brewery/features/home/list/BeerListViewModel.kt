package com.sample.brewery.features.home.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.sample.brewery.data.BeerItemsRepository
import kotlinx.coroutines.Dispatchers

class BeerListViewModel(private val beerItemsRepository: BeerItemsRepository) : ViewModel(
) {
    val beerItemsLiveData = liveData(Dispatchers.IO) {
        val response = beerItemsRepository.getBeerItems()
        emit(response)
    }

}
