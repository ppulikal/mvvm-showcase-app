package com.sample.brewery.features.home.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.sample.brewery.R
import com.sample.brewery.data.Beer
import com.sample.brewery.features.home.list.BeerListViewModel
import kotlinx.android.synthetic.main.beer_detail.*
import kotlinx.android.synthetic.main.beer_detail_ingr_tv.view.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class BeerDetailFragment : Fragment() {

    companion object {
        /**
         * The fragment argument representing the Beer ID that this fragment
         * represents.
         */
        const val ARG_ID = "beer_id"

    }


    private val TAG = "BeerDetailFragment"
    private val viewModel: BeerListViewModel by sharedViewModel() //TODO: for now sharing the list viewmodel, change in future
    private var beer: Beer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) return

        arguments?.let { bundle ->
            if (bundle.containsKey(ARG_ID)) {
                val id = bundle.getInt(ARG_ID)

                Log.d(TAG, "id = $id")
                if (viewModel.beerItemsLiveData.value?.data?.size != 0)
                    beer = viewModel.beerItemsLiveData.value?.data?.first { it.id == id }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.beer_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        Glide.with(img_beer_detail.context)
            .load(beer?.image_url)
            .placeholder(R.drawable.ic_image_black_24dp)
            .into(img_beer_detail)


        beer_detail_name.text = beer?.name
        beer_detail_abv.text = beer?.abv.toString()
        beer_detail_desc.text = beer?.description
        val hopSize = beer?.ingredients?.hops?.size ?: 0
        val maltSize = beer?.ingredients?.malt?.size ?: 0
//        val methodsSize = beer?.method?.size ?: 0


        for (i in 0 until hopSize) layout_beer_detail_ingr.addView(beer?.ingredients?.hops?.get(i)?.name?.let {
            getTextView(
                it
            )
        })

        for (i in 0 until maltSize) layout_beer_detail_malts.addView(beer?.ingredients?.malt?.get(i)?.name?.let {
            getTextView(
                it
            )
        })
    }

    private fun getTextView(text: String): View {
        val view = LayoutInflater.from(context).inflate(R.layout.beer_detail_ingr_tv, null)
        view.ingr_tv.text = text

        return view
    }


}
