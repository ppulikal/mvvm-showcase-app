package com.sample.brewery.features.home.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sample.brewery.R
import com.sample.brewery.data.Beer
import kotlinx.android.synthetic.main.beer_list_content.view.*

class BeerItemsRecyclerViewAdapter(
    var values: List<Beer>,
    private val onClickListener: View.OnClickListener
) :
    RecyclerView.Adapter<BeerItemsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.beer_list_content, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.nameView.text = item.name
        holder.abView.text = item.abv.toString()

        Glide.with(holder.imgView.context)
            .load(item.image_url)
            .placeholder(R.drawable.ic_image_black_24dp)
            .into(holder.imgView)

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameView: TextView = view.name
        val abView: TextView = view.abv
        val imgView: ImageView = view.img
    }

}