package com.sample.brewery.features.home.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.sample.brewery.R
import com.sample.brewery.base.Response
import com.sample.brewery.base.Status
import com.sample.brewery.data.Beer
import com.sample.brewery.features.home.detail.BeerDetailFragment
import com.sample.brewery.utils.navigate
import kotlinx.android.synthetic.main.beer_list.*
import kotlinx.android.synthetic.main.beer_list_fragment.*
import kotlinx.coroutines.delay
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.lang.Thread.sleep

class BeerListFragment : Fragment() {
    private val TAG = "BeerListFragment"

    private var twoPane: Boolean = false
    private val beerListViewModel: BeerListViewModel by sharedViewModel()

    private val observer = Observer<Response<List<Beer>>> {
        when (it.status) {
            Status.ERROR -> showError(it.message ?: "Unknown")
            Status.SUCCESS -> handleListUpdate(it.data ?: emptyList())
            Status.PROGRESS -> showProgress()
        }
    }
    private var beerItemsAdapter: BeerItemsRecyclerViewAdapter? = null
    private val onListItemClickListener: View.OnClickListener

    init {
        onListItemClickListener = View.OnClickListener { v ->
            val item = v.tag as Beer
            if (twoPane) {
                val fragment = BeerDetailFragment().apply {
                    arguments = Bundle().apply {
                        putInt(BeerDetailFragment.ARG_ID, item.id)
                    }
                }
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.beer_detail_container, fragment)?.commit()
            } else {
                showBeerDetails(Bundle().apply {
                    putInt(BeerDetailFragment.ARG_ID, item.id)
                })
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.beer_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        twoPane = (beer_detail_container != null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(beer_list)
        beerListViewModel.beerItemsLiveData.observe(viewLifecycleOwner, observer)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        beerItemsAdapter = beerListViewModel.beerItemsLiveData.value?.let {
            BeerItemsRecyclerViewAdapter(
                it.data ?: emptyList(), onListItemClickListener
            )
        }
        recyclerView.adapter = beerItemsAdapter
    }

    private fun handleListUpdate(it: List<Beer>) {
        Log.d(TAG, "SUCCESS - Inside handleListUpdate - ${it.size}")

        if (it.isNotEmpty()) {
            setupRecyclerView(beer_list)
            beerItemsAdapter?.values = it.sortedBy { it.name }
            beerItemsAdapter?.notifyDataSetChanged()
            hideProgress()
            hideError()
        }
    }

    private fun showBeerDetails(bundle: Bundle) {
        navigate(R.id.detailFragment, bundle)
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progress.visibility = View.INVISIBLE
    }

    private fun hideError() {
        error_tv.visibility = View.INVISIBLE
    }

    private fun showError(message: String) {
        Log.d(TAG, "ERROR - $message")

        hideProgress()
        error_tv.visibility = View.VISIBLE
        error_tv.text = message
        //Show Error Dialog here if required
    }
}
