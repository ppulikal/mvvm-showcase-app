package com.sample.brewery

import android.os.Bundle
import androidx.fragment.app.FragmentActivity

class StartActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_activity)
    }
}