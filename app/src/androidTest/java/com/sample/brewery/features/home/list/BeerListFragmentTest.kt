package com.sample.brewery.features.home.list

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sample.brewery.R
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BeerListFragmentTest {

    private lateinit var navController: TestNavHostController
    private lateinit var directions: NavDirections
    private lateinit var beerListScenario: FragmentScenario<BeerListFragment>

    @Before
    fun setup() {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        navController.setGraph(R.navigation.nav_graph)
        navController.setCurrentDestination(R.id.homeFragment)
        directions = BeerListFragmentDirections.actionHomeFragmentToDetailFragment()
        beerListScenario =
            launchFragmentInContainer<BeerListFragment>(themeResId = R.style.AppTheme)
        beerListScenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.view!!, navController)
        }
    }


    @Test
    fun `launchBeerListFragmentandVerifyUI`() {
        onView(withId(R.id.app_bar)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.beer_list)).check(matches(isDisplayed()))
        onView(withId(R.id.progress)).check(matches(isDisplayed()))
        onView(withId(R.id.error_tv)).check(matches(not(isDisplayed())))
    }
}
