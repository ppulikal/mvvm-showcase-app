package com.sample.brewery.features.home.detail

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sample.brewery.R
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BeerDetailFragmentTest {

    private lateinit var beerDetailScenario: FragmentScenario<BeerDetailFragment>

    @Before
    fun setup() {
        beerDetailScenario =
            launchFragmentInContainer<BeerDetailFragment>(themeResId = R.style.AppTheme)
    }

    @Test
    fun `launchBeerDetailFragmentandVerifyUI`() {
        Espresso.onView(ViewMatchers.withId(R.id.img_beer_detail))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.beer_detail_abv))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.layout_beer_detail_ingr))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.layout_beer_detail_malts))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }
}