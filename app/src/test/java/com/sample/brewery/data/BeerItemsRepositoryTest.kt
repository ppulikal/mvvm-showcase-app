package com.sample.brewery.data

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.sample.brewery.base.Response
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.HttpException

@RunWith(JUnit4::class)
class BeerItemsRepositoryTest {

    private lateinit var beerItemsService: BeerItemsService
    private lateinit var beerItemsRepository: BeerItemsRepository

    private val beerItems = listOf<Beer>(Beer(id = 1, name = "xyz"), Beer(id = 2, name = "abc"))
    private val getBeerItemsSuccessResponse = Response.success(beerItems)
    private val getBeerItemsErrorResponse = Response.error("Unknown Exception", null)
    private lateinit var mockException: HttpException

    @Before
    fun setUp() {
        beerItemsService = mock()
        mockException = mock()
        whenever(mockException.code()).thenReturn(404)

        beerItemsRepository = BeerItemsRepository(
            beerItemsService
        )
    }

    @Test
    fun `test getBeerItems returns Beer objects`() =
        runBlocking {
            whenever(beerItemsService.getBeerItems()).thenReturn(beerItems)
            assertEquals(getBeerItemsSuccessResponse, beerItemsRepository.getBeerItems())
            assertEquals(beerItemsRepository.getBeerItems().data?.size, 2)

        }

    @Test
    fun `test getBeerItems returns error`() =
        runBlocking {
            whenever(beerItemsService.getBeerItems()).thenThrow(mockException)
            assertEquals(
                getBeerItemsErrorResponse.status,
                beerItemsRepository.getBeerItems().status
            )
        }

}