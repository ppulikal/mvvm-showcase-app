package com.sample.brewery.di

import org.junit.Test
import org.junit.experimental.categories.Category
import org.koin.test.AutoCloseKoinTest
import org.koin.test.category.CheckModuleTest
import org.koin.test.check.checkModules

@Category(CheckModuleTest::class)
class ModuleCheckTest : AutoCloseKoinTest() {

    @Test
    fun testModules() = checkModules {
        modules(
            listOf(
                appModules,
                networkModule
            )
        )
    }
}