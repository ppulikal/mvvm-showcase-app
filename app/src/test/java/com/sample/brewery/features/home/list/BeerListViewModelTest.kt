package com.sample.brewery.features.home.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.timeout
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.sample.brewery.base.Response
import com.sample.brewery.data.Beer
import com.sample.brewery.data.BeerItemsRepository
import com.sample.brewery.getOrAwaitValue
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class BeerListViewModelTest {

    private lateinit var viewModel: BeerListViewModel
    private lateinit var beerItemsRepository: BeerItemsRepository
    private lateinit var beerItemsObserver: Observer<Response<List<Beer>>>

    private val getBeerItemsSuccessResponse = Response.success(listOf(Beer(), Beer()))
    private val getBeerItemsErrorResponse = Response.error("Test Error", null)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")


    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        beerItemsRepository = mock()
        viewModel = BeerListViewModel(beerItemsRepository)
        beerItemsObserver = mock()
    }


    @Test
    fun `check if getBeerItems is updated with success`() = runBlocking {
        whenever(beerItemsRepository.getBeerItems()).thenReturn(getBeerItemsSuccessResponse)
        viewModel.beerItemsLiveData.observeForever(beerItemsObserver)
        delay(10)
        beerItemsRepository.getBeerItems()
        verify(beerItemsObserver, timeout(100)).onChanged(getBeerItemsSuccessResponse)
    }



    @Test
    fun `check BeerItems LiveData`() = runBlocking {
        whenever(beerItemsRepository.getBeerItems()).thenReturn(getBeerItemsSuccessResponse)
        viewModel.beerItemsLiveData.observeForever(beerItemsObserver)
        beerItemsRepository.getBeerItems()

        assertEquals(viewModel.beerItemsLiveData.getOrAwaitValue().data?.size, 2)
    }


    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }
}